package twitterMessengerEngine;


import java.time.*;
import java.lang.String;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class Tweet {
	
    private String tweetText = "";//stores text of tweet
    private String username = "";//stores username of Twitter user sending tweet (starts w/ '@')
    private static List<String> mentionEntities = new ArrayList<String>();//holds list of all mentions in tweet (all start w/ '@')
    private static List<String> hashtagEntities = new ArrayList<String>();//holds list of all hashtags in tweet (all start w/ '#')
    private static List<String> URLEntities = new ArrayList<String>();
    public LocalDateTime timePosted;//stores time the tweet was posted
    //private TweetHandler parser;

    public Tweet (String user, String text) {
    	if (!user.contains(" "));//usernames cannot have spaces
    		username = user;
    	if (tweetText.isEmpty() && text.length() <= 140) {//tweet length cannot be more than 140 characters
    		tweetText = text;
    	}
    	timePosted = LocalDateTime.now();
    	
    	parseTweet();
    }
    
    public Tweet () {
    	timePosted = LocalDateTime.now();
    }
    
    public String getUser() {//returns username
        return username;
    }
    
    public String getText() {//return tweetText
    	return tweetText;
    }
    
    public String getTimePosted() {//returns time posted as a string
    	return timePosted.toString();
    }
    
    public String getTimeSincePosted () {//returns difference in minutes between current time & time posted (could be made much more specific)
    	return LocalDateTime
    			.now()
    			.minusMinutes(timePosted.getMinute())
    			.toString();
    }

	public List<String> getURLEntities() {
		return URLEntities;
	}

	public void appendURLEntities(String confirmedURL) {
		//confirmURLs(this, confirmedURL);
		URLEntities.add(confirmedURL);
		
		if (!tweetText.contains(confirmedURL))//if for some reason a hashtag is added to this list,
			//it is added to the tweetTest for consistency
			tweetText.concat(confirmedURL);
	}
	
	//function to set list of URLs for testing purposes
	public void setURLEntities(List<String> potentialURLs) {
		URLEntities = potentialURLs;
	}

	public List<String> getHashtagEntities() {
		return hashtagEntities;
	}

	public void appendHashtagEntities(String confirmedHashtag) {
		if (tweetText.contains(confirmedHashtag))//if for some reason a hashtag is added to this list,
			//it is added to the tweetTest for consistency
			hashtagEntities.add(confirmedHashtag);
	}
	
	//function to set list of Hashtags for testing purposes
	public void setHashtagEntities(List<String> potentialHashtags) {
		hashtagEntities = potentialHashtags;
	}

	public List<String> getMentionEntities() {
		return mentionEntities;
	}

	public void appendMentionEntities(String confirmedMention) {
		if (!tweetText.contains(confirmedMention))//if for some reason a hashtag is added to this list,
			//it is added to the tweetTest for consistency
			mentionEntities.add(confirmedMention);
	}
	
	//function to set list of Mentions for testing purposes
	public void setMentionEntities(List<String> potentialMentions) {
		mentionEntities = potentialMentions;
	}
	
	
	public void parseTweet () {
    	ArrayList<String> tweetArray = new ArrayList<String> (Arrays.asList(getText().split(" ")));
    	boolean confirm = false;
    	
    	for (int i = 0; i < tweetArray.size(); i++) {
    		String temp = tweetArray.get(i).trim();
    		
    		confirm = confirmEntities(temp);
    		
    		if (confirm) {
    			switch (temp.charAt(0)) {
    			case '@'://attempted mention
    				appendMentionEntities(temp);
    				break;
    			case '#'://attempted hashtag
    				appendHashtagEntities(temp);
    				break;
    			default://if not @ or # then has to be URL
    				appendURLEntities(temp);
    				break;
    			}
    		}
    		
    	}
    }
	
	//from TweetHandler class
	public boolean confirmEntities (String potential) {
		String desiredEntity = "", regex = "";//initialized to blank to ensure value

		switch (potential.charAt(0)) {
		case '@'://attempted mention
			//regex set to pattern for mentions
			regex = "([^a-z0-9_!#$%&*@\uFF20]|^|(?:^|[^a-z0-9_+~.-])RT:?)([@\uFF20]+)([a-z0-9_]{1,20})(/[a-z][a-z0-9_\\-]{0,24})?";
			break;
		case '#'://attempted hashtag
			//regex set to pattern for hashtags
			regex = "(^|[^&\\p{L}\\p{M}\\p{Nd}_])(#|\uFF03)(?!\uFE0F|\u20E3)([\\p{L}\\p{M}\\p{Nd}_]*[\\p{L}\\p{M}][\\p{L}\\p{M}\\p{Nd}_]*)";
			break;
		default://if not @ or # then could be URL
			desiredEntity = "URL";
			regex = "<\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]>";//regex set to pattern for URLs
			break;
		}
		
		Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		
		try {
			Pattern.matches(regex, potential);
			if (desiredEntity == "URL")
				pingURL(potential);
		} catch (PatternSyntaxException e1) {
			if (desiredEntity == "URL" && pingURL(potential)) {
				try {
					Pattern.compile("^https?:\\/\\/t\\.co\\/[a-z0-9]+", Pattern.CASE_INSENSITIVE);
					Pattern.matches("^https?:\\/\\/t\\.co\\/[a-z0-9]+", potential);
				} catch (PatternSyntaxException e2) {
					return false;
				}
				return true;
			} else
				return false;
		}
		return true;
	}
	
	//found snippet online for testing URL connections, returns true if valid connection can be made (src = http://crunchify.com/how-to-get-ping-status-of-any-http-end-point-in-java/)
	public static boolean pingURL(String url) {
        try {
            URL siteURL = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) siteURL
                    .openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
 
            int code = connection.getResponseCode();
            if (code == 200) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
};
