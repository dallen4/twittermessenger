package twitterMessengerEngine;

import java.lang.String;
import java.util.regex.*;
import java.net.URL;
import java.net.HttpURLConnection;


public class TweetHandler extends Tweet {	
	
	
	//function to determine whether the substring of the tweet is a URL
	public boolean confirmURLs (Tweet t, String potential) {
		//found regex String to validate URLs (src = http://stackoverflow.com/questions/163360/regular-expression-to-match-urls-in-java)
		String regex = "<\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]>";
		
		Pattern.compile(regex);
		
		try {
			Pattern.matches(regex, (CharSequence) potential);
		} catch (PatternSyntaxException e) {
			try {
				//found regex String to validate shortened URLs (src = http://stackoverflow.com/questions/12790536/regex-to-find-shorturl-in-a-twitter-text-file)
				Pattern.matches("^https?:\\/\\/t\\.co\\/[a-z0-9]+", potential);
			} catch (PatternSyntaxException e1) {
				return false;
			}
			return false;
		}
		t.appendURLEntities(potential);
		return true;
	}
	
	//function to determine whether the substring of the tweet is a valid hashtag
	//ample description found from Twitter support page via http://stackoverflow.com/questions/8451846/actual-twitter-format-for-hashtags-not-your-regex-not-his-code-the-actual
	public boolean confirmHashtags (Tweet t, String potential) {
		//found regex String to validate Hashtags (src = https://github.com/twitter/twitter-text/blob/master/java/src/com/twitter/Regex.java)
		String regex = "(^|[^&\\p{L}\\p{M}\\p{Nd}_])(#|\uFF03)(?!\uFE0F|\u20E3)([\\p{L}\\p{M}\\p{Nd}_]*[\\p{L}\\p{M}][\\p{L}\\p{M}\\p{Nd}_]*)";
		Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		
		try {
			Pattern.matches(regex, potential);
		} catch (PatternSyntaxException e) {
			return false;
		}
		t.appendHashtagEntities(potential);
		return true;
	}
	
	
	//function to determine whether the substring of the tweet is a valid Twitter handle (username)
	public boolean confirmMention (Tweet t, String potential) {
		//found regex String to validate Mentions (src = https://github.com/twitter/twitter-text/blob/master/java/src/com/twitter/Regex.java)
		String regex = "([^a-z0-9_!#$%&*@\uFF20]|^|(?:^|[^a-z0-9_+~.-])RT:?)([@\uFF20]+)([a-z0-9_]{1,20})(/[a-z][a-z0-9_\\-]{0,24})?";
		Pattern.compile(regex);
		
		try {
			Pattern.matches(regex, potential);
		} catch (PatternSyntaxException e) {
			return false;
		}
		t.appendMentionEntities(potential);
		return true;
	}
	/*
	//After seeing redundancies in the above three functions, thought to try to consolidate basic functionality
	public boolean confirmEntities (Tweet t, String potential) {
		String desiredEntity = "", regex = "";//initialized to blank to ensure value

		switch (potential.charAt(0)) {
		case '@'://attempted mention
			//regex set to pattern for mentions
			regex = "([^a-z0-9_!#$%&*@\uFF20]|^|(?:^|[^a-z0-9_+~.-])RT:?)([@\uFF20]+)([a-z0-9_]{1,20})(/[a-z][a-z0-9_\\-]{0,24})?";
			break;
		case '#'://attempted hashtag
			//regex set to pattern for hashtags
			regex = "(^|[^&\\p{L}\\p{M}\\p{Nd}_])(#|\uFF03)(?!\uFE0F|\u20E3)([\\p{L}\\p{M}\\p{Nd}_]*[\\p{L}\\p{M}][\\p{L}\\p{M}\\p{Nd}_]*)";
			break;
		default://if not @ or # then could be URL
			desiredEntity = "URL";
			regex = "<\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]>";//regex set to pattern for URLs
			break;
		}
		
		Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		
		try {
			Pattern.matches(regex, (CharSequence) potential);
			if (desiredEntity == "URL")
				pingURL(potential);
		} catch (PatternSyntaxException e1) {
			if (desiredEntity == "URL" && pingURL(potential)) {
				try {
					Pattern.compile("^https?:\\/\\/t\\.co\\/[a-z0-9]+", Pattern.CASE_INSENSITIVE);
					Pattern.matches("^https?:\\/\\/t\\.co\\/[a-z0-9]+", potential);
				} catch (PatternSyntaxException e2) {
					return false;
				}
				return true;
			} else
				return false;
		}
		return true;
	}
	*/
	//found snippet online for testing URL connections, returns true if valid connection can be made (src = http://crunchify.com/how-to-get-ping-status-of-any-http-end-point-in-java/)
	public static boolean pingURL(String url) {
        try {
            URL siteURL = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) siteURL
                    .openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
 
            int code = connection.getResponseCode();
            if (code == 200) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
 	
}
