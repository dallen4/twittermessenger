package twitterMessengerEngine;


import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.List;
import java.util.regex.Pattern;

public class TweetTest {	
	
	@Before
	public void setUp() throws Exception {
		Tweet simpleTweet = new Tweet ("@nieky_allen", "This is my #firstTweet! @dallen4 https://www.google.com");
		assertEquals(simpleTweet.getUser(),"@nieky_allen");
		assertEquals(simpleTweet.getText(), "This is my #firstTweet! @dallen4 https://www.google.com");
		simpleTweet.parseTweet();
		assertTrue(simpleTweet.getHashtagEntities().size() == 1);
		assertTrue(simpleTweet.getURLEntities().size() == 1);
		assertTrue(simpleTweet.getMentionEntities().size() == 1);
				
	}
	
	@Test
	public void testTweetLength () {
		Tweet simpleTweet = new Tweet ("@nieky_allen", "This is my #firstTweet! @dallen4 https://www.google.com");
		assertTrue(simpleTweet.getText().length() <= 140);
//		assertTrue(errorTweet.getText().length() <= 140);
	}
	
	@Test
	public void testHashtags () {
		Tweet simpleTweet = new Tweet ("@nieky_allen", "This is my #firstTweet! @dallen4 https://www.google.com");
		List<String> hashtags1 = simpleTweet.getHashtagEntities();
		Pattern.compile("[#]+([A-Za-z]+[A-Za-z0-9-_]*)", Pattern.CASE_INSENSITIVE);
		
		boolean temp = false;
		
		for (int i = 0; i < hashtags1.size(); i++) {
			assertTrue(hashtags1.get(i).charAt(0) == '#');
			temp = Pattern.matches("[#]+([A-Za-z]+[A-Za-z0-9-_]*)", hashtags1.get(i));
			//assertTrue(temp);
		}
		
	}
	
	@Test
	public void testMentions () {
		Tweet simpleTweet = new Tweet ("@nieky_allen", "This is my #firstTweet! @dallen4 https://www.google.com");
		List<String> mentions1 = simpleTweet.getMentionEntities();
		Pattern.compile("([^a-z0-9_!#$%&*@\uFF20]|^|(?:^|[^a-z0-9_+~.-])RT:?)([@\uFF20]+)([a-z0-9_]{1,20})(/[a-z][a-z0-9_\\-]{0,24})?", Pattern.CASE_INSENSITIVE);
		
		boolean temp = false;

		for (int i = 0; i < mentions1.size(); i++) {
			assertTrue(mentions1.get(i).charAt(0) == '@');
			assertTrue(simpleTweet.getText().contains(mentions1.get(i)));
			temp = Pattern.matches("([^a-z0-9_!#$%&*@\uFF20]|^|(?:^|[^a-z0-9_+~.-])RT:?)([@\uFF20]+)([a-z0-9_]{1,20})(/[a-z][a-z0-9_\\-]{0,24})?", mentions1.get(i));
			assertTrue(temp);
		}
	}
	
	@Test
	public void testURLs () {
		Tweet simpleTweet = new Tweet ("@nieky_allen", "This is my #firstTweet! @dallen4 https://www.google.com");
		List<String> URLs1 = simpleTweet.getMentionEntities();
		Pattern.compile("<\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]>", Pattern.CASE_INSENSITIVE);
		
		boolean temp = false;
		
		for (int i = 0; i < URLs1.size(); i++) {
			temp = Pattern.matches("<\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]>", URLs1.get(i));
			assertTrue(temp);
		}
	}
	
	@After
	public void tearDown() throws Exception {
		
	}

}
