#TwitterMessenger Project#

## Purpose ##

The purpose of this project was the create classes to store the data and attributes of a Tweet (or something similar). The classes are designed to parse the Tweet text content and confirm/deny whether certain substrings were Mentions, Hashtags, URLs, or just content. After being identified, they are stored in Lists to be used for analytics, tweet identification, etc. later.
A secondary class is present (testTweet) to test the success of creating and parsing the Tweet object. As of now, there are problems with a NullPointerException that I am in the process of debugging, haven't figured it out yet.

## Some References ##

* [General Regex](http://www.tutorialspoint.com/java/java_regular_expressions.htm)
* [Pattern Class](https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html)
* [General Hashtag Format](http://stackoverflow.com/questions/8451846/actual-twitter-format-for-hashtags-not-your-regex-not-his-code-the-actual)
* [Tweet Entity Regexes](https://github.com/twitter/twitter-text/blob/master/java/src/com/twitter/Regex.java)